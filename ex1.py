import pygame, sys, random
from pygame.math import Vector2
import lib

# Global variables
BLACK = (0, 0, 0) # defining the black color using RBG
WHITE = (255, 255, 255) # defining the white color using RBG
GREEN = (0, 83, 19) # defining the green color using RBG
WINDOW_HEIGHT = 600 # defining the height of the game window in pixels 
WINDOW_WIDTH = 600 # defining the width of the game window in pixels
BLOCK_SIZE = 50 #Set the size of the grid block
MAZE = lib.maze.predefined_maze() # call this function to get the maze matrix, which will be used to draw the maze on the window, and will also be used by the player to navigate the maze.

class Player(pygame.sprite.Sprite):
    """
    This is the Player class, which is used to define the main functionalities of the player.
    """
    def __init__(self,
                 pos = Vector2(BLOCK_SIZE, BLOCK_SIZE)):
        """ This is the class constructor """
        super().__init__()
        self.pos=pos # The position of the player
        self.image = pygame.image.load("graphics/robot.png").convert() # Load the player's image
        self.image = pygame.transform.scale(self.image, (BLOCK_SIZE, BLOCK_SIZE)) # set the size to fit the size of the maze cells = BLOCK_SIZE
        self.rect = self.image.get_rect(center=self.pos) # Create a rectangle around the image to use is late for collision detection.
        self.image.set_colorkey(BLACK) # Adjust the image transparency
        self.visited_pos = [] # Initialize an empty list to store the visited cells at a later stage

    def explore(self):
        """This function is responsible for the movement of the player"""

        ## YOUR CODE GOES HERE
        pass
    
    def get_available_moves(self, neighbors_list):
        """ This method will return a list of valid moves given a neighbors list. The valid moves should be where there is a neighboring empty cells (with values 0)
        """
        all_moves = [self.move_right, self.move_left, self.move_up, self.move_down] # list of all the possible moves

        ## YOUR CODE GOES HERE
        pass

    def get_neighbors(self):
        """
        This function will return the values of all the neighbors of the robot in a list in the following order: [Right, Left, Up, Down]
        """
        r = int(self.pos.y/50) # get the row index (r), PAY ATTENTION that it is equivalent to the y position on the screen
        c = int(self.pos.x/50) # get the column index (c), PAY ATTENTION that it is equivalent to the x position on the screen
        return [MAZE[r][c+1], MAZE[r][c-1], MAZE[r-1][c], MAZE[r+1][c]]
    
    def move_right(self):
        """Function to move to the right"""
        self.pos = self.pos + Vector2((BLOCK_SIZE,0))

    def move_left(self):
        """Function to move to the left"""
        self.pos = self.pos + Vector2((-BLOCK_SIZE,0))

    def move_up(self):
        """Function to move up"""
        self.pos = self.pos + Vector2((0,-BLOCK_SIZE))

    def move_down(self):
        """Function to move down"""
        self.pos = self.pos + Vector2((0,BLOCK_SIZE))

    def checkCollision(self, sprite2):
        """Function to check the collision with the target"""
        if self.rect.colliderect(sprite2):
            print("You Win!")
            sys.exit()

class Target(pygame.sprite.Sprite):
    """
    This is the Target class, which is used to define the main functionalities of the player.
    """
    def __init__(self,pos=Vector2((500,450))):
        super().__init__()
        self.pos = pos
        self.image = pygame.image.load("graphics/target.png").convert()
        self.image = pygame.transform.scale(self.image, (BLOCK_SIZE, BLOCK_SIZE))
        self.rect = self.image.get_rect(center=self.pos)
        self.image.set_colorkey((0, 0, 0))

def main():
    global SCREEN, CLOCK
    pygame.init() # Initialize the game
    SCREEN = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT)) # Set the screen height and width
    CLOCK = pygame.time.Clock() # Define the clock, you can adjust it to speed-up or slow-down the game
    
    player = Player() # Create and instance of the class Player
    target = Target() # Create and instance of the class Target

    while True: # Start the main loop of the game
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        SCREEN.fill(WHITE)
        player.explore() # invoke the explore function for the player to start moving
        lib.maze.drawMaze(SCREEN, MAZE, BLOCK_SIZE) # Call the drawMaze method from the lib package to draw the maze on the screen
        SCREEN.blit(player.image,player.pos) # Show the player's position on the screen.
        SCREEN.blit(target.image,target.pos) # Show the target's position on the screen.
        player.checkCollision(target.rect) # Check the collision with the target, to see if you solved the maze successfully.
        pygame.display.flip()
        CLOCK.tick(5)

if __name__ == '__main__':
    main()