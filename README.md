# Final Exam

This is the final exam for the course Advanced Python Programming.

**Before you start the exam:**

Clone this repository to your local working directory using:
``` sh
git clone REMOTE_ADRESS
```

**Any push to this repository will be considered cheating and will lead to canceling the exam directly**

## Exercise 1

In this exercise, you are going to make a robot move in a maze from a starting position to a target position. The maze's map, the starting and the target positions are all fixed as shown in the figure below:

![](graphics/ex1-1.png)

You are given a code that implements all the backbone of the game. It generates the maze using the package `lib.maze`.

This code also contains two main classes inheriting from `pygame.sprite.Sprite`, the class `Player` and the class `Target`.

* The class `Player`: It is the class responsible for the player's appearance and movements. It contains the following methods:
  * `__init__`: The constructor, it initializes an instance of the class.
  * `explore` : A method responsible for the movement of the robot
  * `get_available_moves`: A method responsible for returning all the available move at a certain positions. This function checks if there is walls in the the neighboring cells and eliminate the possibility of moving towards them.
  * `get_neighbors`: A function that returns all the neighbors of the player at a given position (see the figure below to understand how it functions).
  * `checkCollision`: Checks if the player collided with the target.
  * `move_right`
  * `move_left`
  * `move_up`
  * `move_down`

![](graphics/directions.png)

* The class `Target`: It is the class responsible for the target's appearance and position. In this exercise, the target doesn't move, it will appear always on the same exact position of the maze.

The `main` function contains the necessary routines to run the pygame window, to create instances of the `Player` and `Target` classes and to call the methods responsible for the movements.

Our goal is to make the robot move along the maze until it arrives at destination, an illustration of what the final outcome might be is given in the animation below:

![](animations/ex1.gif)

To achieve this goal, you will have to fill the two methods in the `Player` class:

* `get_available_moves`: Given a `neighbors_list`, **return all the available moves from the list of available moves**.

> The zeros in the `neighbors_list` corresponds to the empty cells (no walls), and the cells with value "1" are considered walls as illustrated in the figure below:

![](graphics/matrix-move.png)


* `explore`: You should implement the following in order:
  1. The maze is a matrix,  you can check it by printing its value. Each position of the player on the screen corresponds to a given cell in the matrix (row and column indexes). **Get these indexes given the player's position**.
  2. Mark the cell in the maze as already visited (you can fill the cell with a value = -1)
  3. From the list of available moves, choose randomly one move and call it.

> Take your time in reading and understanding the code, it will be beneficial for both exercises.


## Exercise 2

This exercises uses the same structure as the first one, but this time, the maze will be generated randomly, and the target will appear at a random position as shown in the figure below:

![](graphics/ex2-1.png)

In this case, the `Player` should move in a more intelligent way to reach the target. The end goal is to achieve the scenario in the following animation:

![](animations/ex2.gif)

To help you achieve this goal, you can use the following hints:

* As it moves, the robot will always checks for available moves using the function `get_available_moves`. You can mark the already visited cells in the matrix as "-1", instead of "0" (empty cell) or "1" (Wall). This way, you can keep track of the cells you already visited.
You can also use an array or list to store the visited positions.

* Use `if-elif-else` statements to test different scenarios.

### Bonus
When the player arrives to destination, it is not guaranteed that it took the shortest possible path.
Can you come up with a way to make the robot take the shortest path when he runs the same maze again?