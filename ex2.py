import pygame, sys, random
from pygame.math import Vector2
import lib

# Global variables
BLACK = (0, 0, 0) # defining the black color using RBG
WHITE = (255, 255, 255) # defining the white color using RBG
GREEN = (0, 83, 19) # defining the green color using RBG
WINDOW_HEIGHT = 600 # defining the height of the game window in pixels 
WINDOW_WIDTH = 600 # defining the width of the game window in pixels
BLOCK_SIZE = 50 #Set the size of the grid block
MAZE = lib.maze.predefined_maze() # call this function to get the maze matrix, which will be used to draw the maze on the window, and will also be used by the robot to navigate the maze.
MAZE = lib.maze.randomMaze(int(WINDOW_HEIGHT/BLOCK_SIZE), int(WINDOW_WIDTH/BLOCK_SIZE))

class Player(pygame.sprite.Sprite):
    """
    This is the Player class, which is used to define the main functionalities of the player.
    """
    def __init__(self,
                 pos = Vector2(BLOCK_SIZE, BLOCK_SIZE)):
        """ This is the class constructor """
        super().__init__()
        self.pos=pos # The position of the player
        self.image = pygame.image.load("graphics/robot.png").convert() # Load the player's image
        self.image = pygame.transform.scale(self.image, (BLOCK_SIZE, BLOCK_SIZE)) # set the size to fit the size of the maze cells = BLOCK_SIZE
        self.rect = self.image.get_rect(center=self.pos) # Create a rectangle around the image to use is late for collision detection.
        self.image.set_colorkey(BLACK) # Adjust the image transparency
        self.visited_pos = [] # Initialize an empty list to store the visited cells at a later stage
        self.last_cell_before_dead_end = [] # Initialize an empty list to store the positions of the last cells you visited before reaching a dead end.

    def explore(self):
        """ This function is responsible for the movement of the player"""
        pass

    def get_available_moves(self, neighbors_list, visited):
        """ This method will return a list of valid moves given a neighbors list. The valid moves should be where there is a neighboring empty cells (with values 0)
        """
        all_moves = [self.move_right, self.move_left, self.move_up, self.move_down]
        pass

    def get_neighbors(self):
        """
        This function will return the values of all the neighbors of the robot in a list in the following order: [Right, Left, Up, Down]
        """
        r = int(self.pos.y/50) # get the row index (r), PAY ATTENTION that it is equivalent to the y position on the screen
        c = int(self.pos.x/50) # get the column index (c), PAY ATTENTION that it is equivalent to the x position on the screen
        return [MAZE[r][c+1], MAZE[r][c-1], MAZE[r-1][c], MAZE[r+1][c]]
    
    def move_right(self):
        self.pos = self.pos + Vector2((BLOCK_SIZE,0))

    def move_left(self):
        self.pos = self.pos + Vector2((-BLOCK_SIZE,0))

    def move_up(self):
        self.pos = self.pos + Vector2((0,-BLOCK_SIZE))

    def move_down(self):
        self.pos = self.pos + Vector2((0,BLOCK_SIZE))

    def checkCollision(self, sprite2):
        col = self.rect.colliderect(sprite2)
        if col == True:
            print("You Win!")
            sys.exit()

class Target(pygame.sprite.Sprite):
    def __init__(self,pos):
        super().__init__()
        self.pos = random.choice(self.get_available_cells())
        self.image = pygame.image.load("graphics/target.png").convert()
        self.image = pygame.transform.scale(self.image, (BLOCK_SIZE, BLOCK_SIZE))
        self.rect = self.image.get_rect(center=self.pos)
        self.image.set_colorkey((0, 0, 0))

    def get_available_cells(self):
        available_cells = []
        for index_row, row in enumerate(MAZE):
            for index_col, elem in enumerate(row):
                if elem == 0:
                     available_cells.append(Vector2((index_col*BLOCK_SIZE, index_row*BLOCK_SIZE)))
        return available_cells

def main():
    global SCREEN, CLOCK
    pygame.init()
    SCREEN = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    CLOCK = pygame.time.Clock()
    
    player = Player()
    target = Target((random.randrange(BLOCK_SIZE, WINDOW_WIDTH, BLOCK_SIZE),random.randrange(BLOCK_SIZE, WINDOW_HEIGHT, BLOCK_SIZE)))

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        SCREEN.fill(WHITE)
        player.explore()
        lib.maze.drawMaze(SCREEN, MAZE, BLOCK_SIZE) # Call the drawMaze method from the lib package to draw the maze on the screen 
        SCREEN.blit(target.image,target.pos)
        SCREEN.blit(player.image,player.pos)
        player.checkCollision(target.rect)
        pygame.display.flip()
        CLOCK.tick(3)

if __name__ == '__main__':
    main()